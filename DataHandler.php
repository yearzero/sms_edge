<?php
include_once('DB.php');

class DataHandler
{
    private $date_from;
    private $date_to;
    private $cnt_id;
    private $usr_id;

    public function __construct($date_from, $date_to, $usr_id = null, $cnt_id = null)
    {
        $this->date_from = $date_from ?? null;
        $this->date_to = $date_to ?? null;
        $this->usr_id = $usr_id ?? null;
        $this->cnt_id = $cnt_id ?? null;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function getSuccessFailData()
    {
        if (
            (is_null($this->date_from) || empty($this->date_from)) ||
            (is_null($this->date_to) || empty($this->date_to))) {
            return null;
        }
        $query = "SELECT t.log_created AS `date`,
        SUM(t.log_success) AS `success`,
        COUNT(*) - SUM(t.log_success) AS `no_success`\n";
        //if cnt_id is sent
        $query .= $this->cnt_id ? ", c.cnt_id as `country_id` \n" : "";

        //if usr_id is sent
        $query .= $this->usr_id ? ", t.usr_id as `user_id` \n" : "";

        //concat FROM
        $query .= " FROM smsedge_1.send_log t\n ";

        //cnt_id retrieval
        $query .= $this->cnt_id ? " INNER JOIN numbers n ON t.num_id = n.num_id " : "";
        $query .= $this->cnt_id ? " INNER JOIN countries c ON n.cnt_id = c.cnt_id " : "";

        $query .= $this->usr_id ? "\n WHERE t.usr_id = $this->usr_id  AND " : "";
        $query .= ($this->usr_id ? "" : "\n WHERE ") . " (t.log_created BETWEEN '$this->date_from' AND '$this->date_to') \n";
        $query .= " GROUP BY t.log_created";

        try {
            $db = DB::getInstance();
            DB::setCharsetEncoding();
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
            $stm = $db->prepare($query);
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_ASSOC);

        } catch (Exception $e) {
            print $e->getMessage();
        }
    }

}