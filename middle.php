<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 3/11/2019
 * Time: 1:19 PM
 */
require_once ('DataHandler.php');

//if js is altered.. you shall not pass.
if ($_POST['date_from'] > $_POST['date_to']) {
    return null;
}
echo json_encode((new DataHandler(
    validateDate($_POST['date_from']) ? $_POST['date_from'] : null,
    validateDate($_POST['date_to']) ? $_POST['date_to'] : null,
    $_POST['user_id'],
    $_POST['country_id']
))->getSuccessFailData());

function validateDate($date, $format = 'Y-m-d')
{
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) === $date;
}