#SMS Edge Daily Send Monitor
![picture](https://im4.ezgif.com/tmp/ezgif-4-8d55236473d8.gif)

//
###Installation
1. Run `git clone https://yearzero@bitbucket.org/yearzero/sms_edge.git` in your local environment.
2. Open your browser and goto `%your-local-environment%/smsedge`
3. if you want to use my [SQL mock](https://pastebin.com/h3K2rE6y), feel free.
- I've made FK relations, use user 100 or 1 and country 12 to check the optional params.

Thanks,
Erez.