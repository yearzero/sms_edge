<?php
include('DataHandler.php');
echo "<h1 class='text-center my-4' style='font-family: Roboto, sans-serif'>SMS EDGE Daily Sends</h1>";
echo '<pre>';
//print_r((new DataHandler('10/03/2018','20/02/2019',12,100))->getSuccessFailData());
echo '</pre>';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SMS EDGE PART 2</title>
    <link rel="stylesheet" href="css/bootstrap4.min.css">
</head>
<body>
<div class="container smsedge__input">
    <form action="middle.php" method="post">
        <div class="row dates">
            <div class="col-md-6">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text text-capitalize" id="basic-addon1">date from</span>
                    </div>
                    <input name="date_from"
                           id="date_from"
                           type="date"
                           class="form-control"
                           placeholder="Date"
                           aria-label="Date"
                           aria-describedby="basic-addon1"
                           required="required"
                    >
                </div>
            </div>
            <div class="col-md-6">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text text-capitalize" id="basic-addon1">date to</span>
                    </div>
                    <input name="date_to"
                           id="date_to"
                           type="date"
                           class="form-control"
                           placeholder="Date"
                           aria-label="Date"
                           aria-describedby="basic-addon1"
                           required="required"
                    >
                </div>
            </div>
        </div>
        <div class="row optionals">
            <div class="col-md-12">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text text-capitalize">user ID</span>
                    </div>
                    <input name="user_id"
                           id="user_id"
                           type="number"
                           class="form-control"
                           placeholder="Enter User ID"
                           aria-label="user"
                           aria-describedby="basic-addon1"
                    >
                    <div class="input-group-append">
                        <span class="input-group-text text-capitalize bg-warning text-dark">optional</span>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text text-capitalize">country ID</span>
                    </div>
                    <input name="country_id"
                           id="country_id"
                           type="number"
                           class="form-control"
                           placeholder="Enter Country ID"
                           aria-label="country"
                           aria-describedby="basic-addon1"
                    >
                    <div class="input-group-append ">
                        <span class="input-group-text text-capitalize bg-warning text-dark">optional</span>
                    </div>
                </div>
            </div>
            <div class="col-md-12 input-group mb-3">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
    <div class="text-center my-4 text-danger text-capitalize errors"><h6></h6></div>
    <div class="row smsedge__table"></div>
</div>
<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script>
  let form = $('form');
  let tbody;
  let formData = {};
  let tableSection = $('.smsedge__table');
  let $errors = $('.errors');

  $(document).ready(function () {
    //I always seperate my doc.ready/window.onload/events and IIFE's
    getTable()
  });

  form.on('submit', function (ev) {
    ev.preventDefault();
    $errors.find('h6').text('');

    let date_from = $('#date_from').val();
    let date_to = $('#date_to').val();
    let user_id = $('#user_id').val();
    let country_id = $('#country_id').val();

    let strtDt  = new Date(date_from);
    let endDt  = new Date(date_to);
    if (strtDt > endDt){
      return $errors.find('h6').text('date from cannot be higher than date to');
    }

    formData = {
      date_from,
      date_to,
      user_id,
      country_id
    };

    getTable(formData)
  });

  function getTable(data) {
    if (data === undefined) {
      tableSection.empty();
      tableSection.append(`
                <div class="container h-50">
                    <div class="row align-items-center h-100">
                        <div class="col-6 mx-auto">
                            <h1 class="text-center">Pick your Dates and optional user ID and/or country ID</h1>
                        </div>
                    </div>
                </div>
                `);
      return false;
    }
    tableInjection();
    $.ajax({
      method: 'POST',
      url: 'middle.php',
      data,
      success: function (result) {
        dataSetter(JSON.parse(result));
        console.log(JSON.parse(result));
        console.log(JSON.parse(result));
      }
    });
  }

  function tableInjection(noData) {
    tableSection.empty();
    if (noData){

    }

    tableSection.append(`
    <table id="theTable" class="table table-dark border text-capitalize text-center">
            <thead class="thead-dark">
            <tr>
                <th scope="col">date</th>
                <th scope="col">success</th>
                <th scope="col">fail</th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    `);
    tbody = $('#theTable').find('tbody');
  }

  function dataSetter(data) {
    // let userExist = Object.keys(formData.user_id).length > 0;
    // let countryExist = Object.keys(formData.country_id).length > 0;
    if (data.length === 0) {
      tbody.append(`

      `);
    }

    //quickest loop
    for (let i = 0; i < data.length; i++) {
      rowFactory(data[i]);
    }
  }

  function rowFactory(row) {
    tbody.append(`
            <tr>
                <td>${row.date}</td>
                <td>${row.success}</td>
                <td>${row.no_success}</td>
            </tr>
    `);
  }
</script>
</body>
</html>
